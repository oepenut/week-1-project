import {
  setErrorFor,
  setSuccessFor,
  isEmail,
  isPasswordValid,
  isPhoneNumber,
  isNumber,
  isURL,
  setExperienceLevel,
} from './utils.js';

const form = document.getElementById('form');
const firstNameInput = document.getElementById('firstName');
const lastNameInput = document.getElementById('lastName');
const emailInput = document.getElementById('email');
const phoneInput = document.getElementById('phone');
const passwordInput = document.getElementById('password');
const confirmPasswordInput = document.getElementById('confirmPassword');
const ageInput = document.getElementById('age');
const websiteInput = document.getElementById('website');
const experienceInput = document.getElementById('experience');
const acceptInput = document.getElementById('accept');
const levelSpan = document.getElementById('level');
const sliderDiv = document.getElementById('slider');
let experienceText = '';
let isExperienceValid = false;

form.addEventListener('submit', (e) => {
  e.preventDefault();
  checkInputs();
  if (!isFormValid()) {
    e.preventDefault();
  } else {
    e.preventDefault();
    const result = {
      firstName: firstNameInput.value.trim(),
      lastName: lastNameInput.value.trim(),
      email: emailInput.value.trim(),
      phone: phoneInput.value.trim(),
      password: passwordInput.value.trim(),
      confirmPassword: confirmPasswordInput.value.trim(),
      age: ageInput.value.trim(),
      website: websiteInput.value.trim(),
      termsAccepted: acceptInput.checked,
      experience: experienceText,
    };
    console.log(result);
  }
});

experienceInput.oninput = () => {
  experienceText = setExperienceLevel(experienceInput, experienceInput.value);
  levelSpan.innerText = experienceText;
  isExperienceValid = true;
};

function isFormValid() {
  const inputContainers = form.querySelectorAll('.input-box');
  const checkBoxContainer = form.querySelector('.terms-conditions');
  let isFormValid = true;
  inputContainers.forEach((container) => {
    if (container.classList.contains('error')) {
      isFormValid = false;
    }
  });
  if (!isExperienceValid) {
    isFormValid = false;
  }
  if (checkBoxContainer.classList.contains('error')) {
    isFormValid = false;
  }
  return isFormValid;
}

function checkInputs() {
  const firstNameInputValue = firstNameInput.value.trim();
  const lastNameInputValue = lastNameInput.value.trim();
  const emailInputValue = emailInput.value.trim();
  const phoneInputValue = phoneInput.value.trim();
  const passwordInputValue = passwordInput.value.trim();
  const confirmPasswordInputValue = confirmPasswordInput.value.trim();
  const ageInputValue = ageInput.value.trim();
  const websiteInputValue = websiteInput.value.trim();
  const isAccepted = acceptInput.checked;

  if (firstNameInputValue === '') {
    setErrorFor(firstNameInput, 'Required Field');
  } else if (firstNameInputValue.length < 4) {
    setErrorFor(firstNameInput, 'First Name must be longer than 3 letters');
  } else {
    setSuccessFor(firstNameInput);
  }

  if (lastNameInputValue === '') {
    setErrorFor(lastNameInput, 'Required Field');
  } else if (lastNameInputValue.length < 4) {
    setErrorFor(lastNameInput, 'Last Name must be longer than 3 letters');
  } else {
    setSuccessFor(lastNameInput);
  }

  if (emailInputValue === '') {
    setErrorFor(emailInput, 'Required Field');
  } else if (!isEmail(emailInputValue)) {
    setErrorFor(emailInput, 'Enter a valid email address');
  } else {
    setSuccessFor(emailInput);
  }

  if (phoneInputValue === '') {
    setErrorFor(phoneInput, 'Required Field');
  } else if (!isPhoneNumber(phoneInputValue)) {
    setErrorFor(phoneInput, 'Enter a valid  phone  number');
  } else {
    setSuccessFor(phoneInput);
  }

  if (passwordInputValue === '') {
    setErrorFor(passwordInput, 'Required Field');
  } else if (!isPasswordValid(passwordInputValue)) {
    setErrorFor(
      passwordInput,
      'Must be longer than 8 character, contain 1 number and 1 special character',
    );
  } else {
    setSuccessFor(passwordInput);
  }

  if (confirmPasswordInputValue === '') {
    setErrorFor(confirmPasswordInput, 'Required Field');
  } else if (passwordInputValue !== confirmPasswordInputValue) {
    setErrorFor(confirmPasswordInput, 'Passwords does not match');
  } else {
    setSuccessFor(confirmPasswordInput);
  }

  if (ageInputValue === '') {
    setErrorFor(ageInput, 'Enter a number');
  } else if (!isNumber(ageInputValue)) {
    setErrorFor(ageInput, 'Enter a number');
  } else {
    setSuccessFor(ageInput);
  }

  if (websiteInputValue === '') {
    setErrorFor(websiteInput, 'Required Field');
  } else if (!isURL(websiteInputValue)) {
    setErrorFor(websiteInput, 'Enter a valid URL, remember the https:// ');
  } else {
    setSuccessFor(websiteInput);
  }

  if (!isExperienceValid) {
    sliderDiv.classList.add('error');
  }
  if (!isAccepted) {
    const parent = document.getElementById('terms-conditions');
    parent.classList.add('error');
    const small = parent.querySelector('small');
    small.innerText = 'You must agree with terms and conditions';
  } else {
    const parent = document.getElementById('terms-conditions');
    if (parent.classList.contains('error')) {
      parent.classList.remove('error');
    }
  }

  return { test: setExperienceLevel(experienceInput.value) };
}
