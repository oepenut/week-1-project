const phaseTwoDiv = document.getElementById('phase-two');
const closeDiv = document.getElementById('close');

const pattern = ['n', 'i', 'c', 'e', 't', 'r', 'i', 'c', 'k'];
let current = 0;

closeDiv?.addEventListener('click', () => {
  phaseTwoDiv.classList.remove('open');
});
const keyHandler = function (event) {
  if (pattern.indexOf(event.key) < 0 || event.key !== pattern[current]) {
    current = 0;
    return;
  }
  current++;
  if (pattern.length === current) {
    current = 0;
    phaseTwoDiv.classList.add('open');
  }
};

document.addEventListener('keydown', keyHandler, false);
